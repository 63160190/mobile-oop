import 'Cars.dart';
import 'Mitsubishi.dart';
import 'Nissan.dart';
import 'Porsche.dart';

void main(List<String> args) {
  Nissan GTR =
      new Nissan("Nissan", "GTR R35 ", "3.8L VR38DETT", "White ", 592, 315);
  GTR.speak();
  GTR.drive();
  GTR.drivespeed();

  Porsche p911 = new Porsche("Porsche ", "911",
      "3.8 L Porsche MA1.75/MDG.GA Flat-6 ", "Red ", 592, 315);
  p911.speak();
  p911.drive();
  p911.drivespeed();

  Mitsubishi evox = new Mitsubishi("Mitsubishi ", "Lancer Evolution X",
      "2.0 L 4B11T I4 Turbocharged ", "Black ", 280, 249);
  evox.speak();
  evox.drive();
  evox.drivespeed();
}

import "dart:io";

class Cars {
  String? name;
  String? model;
  String? engine;
  String? color;
  int hoserpow = 0;
  int topspeed = 0;

  Cars(String name, String model, String engine, String color, int hoserpow,
      int topspeed) {
    this.name = name;
    this.model = model;
    this.engine = engine;
    this.color = color;
    this.hoserpow = hoserpow;
    this.topspeed = topspeed;
  }
  void speak() {
    print(("name: " +
        this.name.toString() +
        "\n" +
        "model: " +
        this.model.toString() +
        "\n" +
        "engine: " +
        this.engine.toString() +
        "\n" +
        "color: " +
        this.color.toString() +
        "\n" +
        "hoserpow: " +
        this.hoserpow.toString() +
        "\n" +
        "topspeed: " +
        this.topspeed.toString() +
        "Km/h"));
  }

  void drive() {}
  void drivespeed() {}
}
